import os


def delete_file_copies(dir):
    my_l = []
    for root, dirs, files in os.walk(dir):
        for file in files:
            my_dict = {}
            my_dict['name'] = os.path.basename(file)
            my_dict['path'] = os.path.join(root, file)
            my_dict['size'] = os.stat(my_dict['path']).st_size
            my_l.append(my_dict)
    i = 0
    j = 1
    while i < len(my_l) + 1:
        while j < len(my_l):
            x = my_compare(my_l[i], my_l[j])
            if x:
                try:
                    os.remove(my_l[j]['path'])
                    del my_l[j]
                    j -= 1
                except:
                    open('plik.txt', 'a+').write(str(my_l[i]['path']) + ' ' + str(my_l[j]['path']) + '\n')
            j += 1
        i += 1
        j = i + 1


def my_compare(file_a, file_b):
    if file_a['name'] == file_b['name'] and file_a['size'] == file_b['size']:
        return True
    return False


def delete_all_empty_dirs(dir):
    for root, dirs, files in os.walk(dir):
        for direct in dirs:
            if len(os.listdir(os.path.join(root, direct))) == 0:
                try:
                    os.rmdir(os.path.join(root, direct))
                except:
                    open('f1.txt', 'a+').write(os.path.join(root, direct))
                print(os.path.join(root, direct))


if __name__ == "__main__":
    # my dir - main directory to be scanned and changed along with it's subdirectories
    my_dir = 'F:\P'
    delete_file_copies(my_dir)
    delete_all_empty_dirs(my_dir)
